# parsely-bot

Simple Python bot used for the online radio playing game Parsely Game "Nom de Zeus"

[FR]

Il s'agit du bot utilisé lors dfe l'émission de radio du 23 avril. 
Principe général : le bot lit les messages IRC diffusés sur le channel.
Les messages commençant par `$<verb>` sont considérés comme des commandes valides,
et sont affichés sur la console (`stdout`).
Les autres commandes envoyés par la suite sont ignorées.
Le joueur-animateur-ordinateur réautorise une nouvelle commande avec n'importe quel
message commençant par `ok`.

## Pour lancer le bot :

- renseigner le serveur / channel / admin_nick en constantes dans le script

- et hop un petit coup de python 3 : `python3 ird_bot.py`