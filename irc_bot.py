import socket
import sys
import time


SERVER = "irc.server.name" # server name
CHANNEL = "#channel" # channel name
BOTNICK = "ParselyBot"
ADMIN = "adminnick" #the nickname of the game manager

PORT = 6667

class IRC:

    irc = socket.socket()

    def __init__(self):
        # Define the socket
        self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.irc.settimeout(1)
        self.is_joined = False
        self.locked = True
        self.started = False

    def send(self, channel, msg):
        # Transfer data
        # self.irc.send(bytes("PRIVMSG " + channel + " " + msg + "\n", "UTF-8"))
        self.irc.send(f"PRIVMSG {channel} :{msg}\n".encode())

    def connect(self, server, port, channel, botnick):
        # Connect to the server
        print("Connecting to: " + server)
        self.irc.connect((server, port))

        # Perform user authentication
        self.irc.send(bytes("USER " + botnick + " " + botnick +" " + botnick + " :python\n", "UTF-8"))
        self.irc.send(bytes("NICK " + botnick + "\n", "UTF-8"))
        # self.irc.send(bytes("NICKSERV IDENTIFY " + botnickpass + " " + botpass + "\n", "UTF-8"))
        time.sleep(5)

        while (self.get_response() is not None):
          continue
        self.join(channel)

    def join(self, channel):
        # join the channel
        time.sleep(1)
        self.irc.send(bytes("JOIN " + channel + "\n", "UTF-8"))
        self.is_joined = True

        time.sleep(1)

        self.channel = channel
        self.send(channel, "PARSELY BOT IS IN THE PLACE !")

    def get_response(self):
        time.sleep(1)
        # Get the response
        try:
            resp = self.irc.recv(2048).decode("UTF-8")
        except:
            return None


        for l in resp.split("\n"):

          if l.startswith("PING"):
              ack = l.replace("PING", "PONG")
              print("sending", ack)
              self.irc.send(ack.encode("UTF-8"))

        return resp

    def get_command(self, msg):
        _, header, message = msg.split(":")

        header_list = header.strip().split()
        msg_list = message.strip().split()

        if header_list[2] != "#radiosansnous":
            return

        if header_list[0].startswith(ADMIN) and msg_list[0] == "ok":
            self.locked = False
            self.started = True
            print("YES MASTER")

        if not self.started:
            print("not started yet")
            return


        if header_list[1] == "PRIVMSG":

            verb = msg_list[0]
            if verb.startswith("$"):


                if self.locked:
                    self.send(self.channel, "CAPACITÉ MÉMOIRE LIMITÉE. ATTENDEZ POUR ENTRER UNE COMMANDE.")
                    return

                if len(verb) > 2 and verb[-2:].lower() in ["er", "ir", "re"]:

                    command = message.strip().upper()[1:]
                    print(command)
                    self.send(self.channel, "COMMANDE ACCEPTÉE POUR LE TRAITEMENT : " + command)

                    self.locked = True

                else:
                    self.send(self.channel, "COMMANDE NON VALIDE")




## Start client here


irc = IRC()
irc.connect(SERVER, PORT, CHANNEL, BOTNICK)

while True:
    text = irc.get_response()
    if text:
        try:
            irc.get_command(text)
        except Exception as e:
            print(e)
